# frozen_string_literal: true

# spec/rails_helper.rb

ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../config/environment', __dir__)

abort('The Rails environment is running in production mode!') if Rails.env.production?
require 'spec_helper'
require 'rspec/rails'
require 'capybara/rails'
require 'company_subdomain.rb'
require './spec/feature_subdomain_helper'
require './spec/request_subdomain_helper'

include Warden::Test::Helpers

Warden.test_mode!

ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = false
  config.before(:suite) { DatabaseCleaner.clean_with(:truncation) }
  config.before(:each) { DatabaseCleaner.strategy = :transaction }
  config.before(:each, js: true) { DatabaseCleaner.strategy = :truncation }
  config.before(:each) { DatabaseCleaner.start }
  config.after(:each) { DatabaseCleaner.clean }
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!
  config.include Rails.application.routes.url_helpers
  config.include Capybara::DSL
  config.include FactoryBot::Syntax::Methods
  config.extend FeatureSubdomainHelper, type: :feature
  config.extend RequestSubdomainHelper, type: :request
end

def visit_path(path, subdomain)
  MyRailsHelper.setting_capybara_host(subdomain: subdomain)
  Capybara.visit path
end

module MyRailsHelper
  def self.setting_capybara_host(subdomain:)
    domain = 'example.com'
    Capybara.app_host = "http://#{subdomain}.#{domain}"
    Capybara.always_include_port = true
  end
end
