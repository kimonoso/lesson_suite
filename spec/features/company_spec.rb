require 'rails_helper'

describe 'Company' do
  let(:user) { FactoryBot.create(:user) }
  let(:company) { user.company }
  let(:other_user) { FactoryBot.create(:other_user) }
  let(:other_company) { other_user.company }

  describe 'home' do
    it 'can be accessed by employees' do
      login_as(user, scope: :user)
      visit root_url(subdomain: company.subdomain)

      expect(page).to have_content(company.name)
    end

    it 'cannot be accessed by non-employees and be redirected to own company' do
      login_as(other_user, scope: :user)
      visit_path(root_path, company.subdomain)

      expect(page).to_not have_content(company.name)
    end
  end
end
