require 'rails_helper'

RSpec.feature 'Lesson' do
  let(:course) { FactoryBot.create(:course) }
  let(:user) { course.user }
  let(:company) { user.company }
  let(:other_user) { FactoryBot.create(:other_user) }
  let(:other_company) { other_user.company }

  describe 'index' do
    it 'can be accessed' do
      login_as(user, scope: :user)

      visit_path(courses_path, company.subdomain)

      expect(page).to have_content(course.name)
    end

    it 'cannot be accessed by non-employees' do
      login_as(other_user, scope: :user)
      visit_path(courses_path, company.subdomain)

      expect(page).to_not have_content(course.name)
    end
  end

  describe 'new' do
    before do
      login_as(user, scope: :user)
    end

    it 'cannot be accessed by non-employees' do
      logout(:user)
      login_as(other_user, scope: :user)

      visit_path(new_course_path, company.subdomain)
      expect(page).to_not have_content('New Course')
    end

    it 'can be can be filled and saved' do
      visit_path(new_course_path, company.subdomain)
      fill_in 'course[name]', with: 'Course 1'
      fill_in 'course[description]', with: 'Course Description'

      expect { click_on('Save') }.to change(Course, :count)
    end

    it 'can create new lessons' do
      visit_path(new_course_path, company.subdomain)
      fill_in 'course[name]', with: 'Course 1'
      fill_in 'course[description]', with: 'Course Description'

      expect { click_on('Add lesson') }.to change(Lesson, :count)
    end
  end
end
