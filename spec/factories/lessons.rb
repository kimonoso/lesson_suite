# frozen_string_literal: true

FactoryBot.define do
  factory :text_lesson, class: 'Lesson' do
    type { 'TextLesson' }
    name { 'Lesson 1' }
    body { 'Body' }
  end
end
