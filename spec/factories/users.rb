FactoryBot.define do
  factory :user do
    first_name { '1 Employee' }
    last_name { 'FullSuite' }
    email { '1@fullsuite.ph' }
    password { 'miko123' }
    password_confirmation { 'miko123' }
    company
  end

  factory :admin, class: 'Admin' do
    first_name { 'Admin' }
    last_name { 'FullSuite' }
    email { 'admin@fullsuite.ph' }
    password { 'miko123' }
    password_confirmation { 'miko123' }
    company
  end

  factory :other_user, class: 'User' do
    first_name  { '1 Kruze Employee' }
    last_name   { 'Kruze Consulting' }
    email       { '1@kruze.com' }
    password    { 'miko123' }
    password_confirmation { 'miko123' }
    association :company, factory: :other_company
  end
end
