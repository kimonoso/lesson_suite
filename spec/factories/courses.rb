# frozen_string_literal: true

FactoryBot.define do
  factory :course do
    name { 'FullSuite Lesson 1' }
    description { 'FullSuite Lesson 1 description' }
    review { 5 }
    review_count { 1 }
    user
  end
end
