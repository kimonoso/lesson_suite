FactoryBot.define do
  factory :company do
    name { 'FullSuite' }
    subdomain { 'fullsuite' }
  end

  factory :other_company, class: 'Company' do
    name { 'Kruze Consulting' }
    subdomain { 'kruzeconsulting' }
  end
end
