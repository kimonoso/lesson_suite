# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Course, type: :model do
  describe 'creation' do
    it 'does not allow repeat course names' do
      FactoryBot.create(:course)
      course = Course.new(
        name: 'FullSuite Course 1',
        description: 'description'
      )
      expect { course.save }.to_not change(Course, :count)
    end
  end
end
