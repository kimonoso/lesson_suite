# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { FactoryBot.build_stubbed(:user) }

  describe 'creation: ' do
    it 'can be created' do
      expect(user).to be_valid
    end

    it 'cannot be created without first_name, last_name' do
      user.first_name = nil
      user.last_name = nil
      expect(user).to_not be_valid
    end
  end

  describe 'custom name method' do
    it 'it has a full name method that combines first name and last name' do
      expect(user.full_name).to eq('1 employee Fullsuite')
    end
  end
end
