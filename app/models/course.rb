class Course < ApplicationRecord
  belongs_to :user
  has_one :company, through: :user
  has_many :lessons, dependent: :destroy

  enum status: {
    draft: 0,
    submitted: 1,
    approved: 2,
    rejected: 3
  }

  validates_presence_of :name
  accepts_nested_attributes_for :lessons
end
