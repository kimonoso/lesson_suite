class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  belongs_to :company

  validates_presence_of :email,
                        :first_name,
                        :last_name

  def full_name
    first_name.capitalize + ' ' + last_name.capitalize
  end
end
