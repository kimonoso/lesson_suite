# frozen_string_literal: true

class Company < ApplicationRecord
  has_many :users

  validates_format_of :subdomain,
                      with: /[a-z0-9_]+/,
                      message: 'must be lowercase alphanumerics only'
  validates_exclusion_of :subdomain,
                         in: %w[www mail ftp],
                         message: 'is not available'
end
