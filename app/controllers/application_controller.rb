# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit
  layout :layout_by_resource
  before_action :authenticate_user!
  before_action :company_workspace

  # rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  #
  # def user_not_authorized
  #   flash[:alert] = 'You are not authorized to perform this action'
  #   redirect_to(root_path)
  # end

  def layout_by_resource
    is_new = action_name == 'new'
    is_create = action_name == 'create'
    is_edit = action_name == 'edit'
    is_update = action_name == 'update'
    is_show = action_name == 'show'

    if devise_controller?
      'devise'
    elsif controller_name = 'react'
      'react'
    else
      'application'
    end
  end

  def company_workspace
    return nil unless current_user

    unless request.subdomain == current_user.company.subdomain
      redirect_to root_url(subdomain: current_user.company.subdomain),
                  notice: "You are not a member of #{request.subdomain} " \
                  'workspace.'
    end
  end

  def after_sign_in_path_for(resource)
    root_url(subdomain: current_user.company.subdomain) ||
      stored_location_for(resource) ||
      welcome_path
  end

  def after_sign_out_path_for(_resource_or_scope)
    root_url
  end
end
