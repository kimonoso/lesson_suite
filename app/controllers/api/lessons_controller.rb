module API
  class LessonsController < ApplicationController
    def index

    end

    def create
      p = params[:lesson]
      @lesson = Lesson.new(
        name: p[:name],
        body: p[:body],
        type: p[:type],
        course_id: p[:course_id]
      )
      @lesson.video = p[:video]

      if @lesson.save
        render json: { params: @lesson, video: @lesson.video }
      else
        render json: { result: 'Failed'}
      end
    end

    def update
      p = params[:lesson]
      @lesson = Lesson.find(:id)

      if @lesson.update
        render json: { params: @lesson }
      else
        render json: { result: 'Failed'}
      end
    end

    def delete

    end

    private

  end
end
