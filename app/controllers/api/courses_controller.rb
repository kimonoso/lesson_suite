# frozen_string_literal: true
module API
  class CoursesController < ApplicationController

    before_action :api_course_params, only: [:create, :show ]

    def index
      @courses = Course.all
      render json: { courses: @courses }
    end

    def new
      @course_form = CourseForm.new
    end

    def create
      @course = Course.new(
        name: @p[:name],
        description: @p[:description],
        user_id: current_user.id
      )
      if @course.save
        render json: { params: @course }
      else
        render json: { result: 'Failed' }
      end
    end

    def show
      @course = Course.find(@p[:id])
      render json: { params: @course }
    end

    def destroy
      @course = Course.find(params[:id])
      if @course.destroy
        render json: { result: 'Deleted Course: ' + @course.name }
      else
        render json: { result: 'Failed deleting Course: ' + @course.name}
      end
    end

    private
    def api_course_params
      @p = params[:course]
    end

    def course_form_params
      params.require(:course).permit(
        course_attributes: %i[name
                              description
                              user_id]
      )
    end
  end
end
