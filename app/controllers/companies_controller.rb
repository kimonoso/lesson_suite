# frozen_string_literal: true

class CompaniesController < ApplicationController
  before_action :set_company

  private

  def set_company
    return nil unless current_user

    @company = current_user.company || not_found
  end

  def not_found
    raise ActionController::RoutingError, 'Company Workspace Not Found'
  end
end
