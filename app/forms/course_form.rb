# frozen_string_literal: true

class CourseForm
  include ActiveModel::Model

  attr_accessor :name, :description, :lesson_attributes, :user_id

  delegate :attributes=, to: :course, prefix: true
  delegate :lessons, to: :course

  validate :course_is_valid

  def initialize(params = {})
    @course = Course.new
    @lessons = @course.lessons.build
    super(params)
    @course.lessons.build if @course.lessons.empty?
  end

  def submit
    @course.attributes = course_params
    return false if invalid?

    @course.save
    # ActiveRecord::Base.transaction do
    #   @invitations.each(&:save!)
    # end

  end

  private

  def course_is_valid
    errors.add(:course, 'has to be unique') if course.invalid?
  end

  def lessons_are_valid
    errors.add(:lessons, 'are invalid') if lessons.any?(&:invalid?)
  end

  def course_params
    {
      name: name,
      description: description,
      lesson_attributes: lesson_attributes
    }
  end
end
