import { FETCH_COURSES,
         FETCH_COURSE,
         NEW_COURSE,
         DELETE_COURSE
       } from '../actions/types';

const initialState = {
  items: [],
  item: {}
}

export default function(state = initialState, action) {
  switch(action.type) {
    case FETCH_COURSE:
      return {
        ...state,
        item: action.payload
      }
    case FETCH_COURSES:
      return {
        ...state,
        items: action.payload
      }
    case DELETE_COURSE:
      return {
        ...state
      }
    case NEW_COURSE:
      return {
        ...state,
        item: action.payload
      }
    default:
      return state;
  }
}
