import { FETCH_LESSONS,
         NEW_LESSON,
         DELETE_LESSON,
         UPDATE_LESSON,
         ADD_VIDEO_LESSON,
         ADD_VIDEO_LESSON2 } from '../actions/types';

const initialState = {
  items: [],
  item: {}
}

export default function(state = initialState, action) {
  switch(action.type) {
    case FETCH_LESSONS:
      return {
        ...state,
        items: action.payload
      }
    case NEW_LESSON:
      return {
        ...state,
        item: action.payload
      }
    case UPDATE_LESSON:
      return {
        ...state,
        item: action.payload
      }
    case DELETE_LESSON:
      return {
        ...state,
        item: action.payload
      }
    case ADD_VIDEO_LESSON:
      return {
        ...state,
        item: action.payload
      }
    case ADD_VIDEO_LESSON2:
      return {
        ...state,
        item: action.payload
      }
    default:
      return state;
  }
}
