import { combineReducers } from 'redux';
import courseReducer from './courseReducer';
import lessonReducer from './lessonReducer';
import toggleReducer from './toggleReducer';

export default combineReducers({
  courses: courseReducer,
  lessons: lessonReducer,
  toggle: toggleReducer
});
