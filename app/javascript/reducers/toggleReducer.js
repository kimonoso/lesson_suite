import { SHOW, HIDE } from '../actions/types';

const initialState = {
  courses: true,
  course: false,
  createCourse: false,
  createLesson: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SHOW:
      return {
        ...state,
        [action.payload]: true
      };
    case HIDE:
      return {
        ...state,
        [action.payload]: false
      };
    default:
      return state;
  }
};
