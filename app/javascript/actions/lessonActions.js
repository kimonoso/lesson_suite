import { FETCH_LESSONS,
         NEW_LESSON,
         DELETE_LESSON,
         UPDATE_LESSON,
         ADD_VIDEO_LESSON } from './types';

import axios from 'axios';

export const fetchLessons = () => dispatch => {
  console.log('fetchLessons');
  axios.get('/api/lessons')
      .then(lessons =>
        dispatch({
          type: FETCH_LESSONS,
          payload: lessons
        })
      )
}

export const createLesson = (lessonData) => dispatch => {
  console.log('createLesson');
  return axios
  .post('/api/lessons', lessonData)
  .then(lesson =>
    dispatch({
      type: NEW_LESSON,
      payload: lesson
    })
  )
}

export const addVideoLesson = (lessonData) => dispatch => {
  return axios
  .post('/api/lessons/', lessonData)
  .then(lesson =>
    dispatch({
      type: ADD_VIDEO_LESSON,
      payload: lesson
    })
  )
}

export const addVideoLesson2 = (lessonData) => dispatch => {
  return axios
  .post('/api/lessons/', lessonData)
  .then(lesson =>
    dispatch({
      type: ADD_VIDEO_LESSON2,
      payload: lesson
    })
  )
}

export const updateLesson = (lessonData) => dispatch => {
  return axios
  .put('/api/lessons/', lessonData)
  .then(lesson =>
    dispatch({
      type: UPDATE_LESSON,
      payload: lesson
    })
  )
}

export const deleteLesson = (lessonId) => dispatch => {
  console.log('deleteLesson');
  axios
  .delete('/api/lessons/${lessonId}')
  .then(lesson =>
    dispatch({
      type: DELETE_LESSON,
    }))
}
