import { SHOW, HIDE } from './types';
export const showToggle = (id) => dispatch => (
  dispatch({
    type: SHOW,
    payload: id
  })
);
export const hideToggle = (id) => dispatch => (
  dispatch({
    type: HIDE,
    payload: id
  })
);
