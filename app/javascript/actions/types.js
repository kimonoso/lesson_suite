export const FETCH_COURSES = 'FETCH_COURSES';
export const NEW_COURSE = 'NEW_COURSE';
export const FETCH_COURSE = 'FETCH_COURSE';
export const DELETE_COURSE = 'DELETE_COURSE';

export const FETCH_LESSONS = 'FETCH_LESSONS';
export const NEW_LESSON = 'NEW_LESSON';
export const DELETE_LESSON = 'DELETE_LESSON';
export const UPDATE_LESSON = 'UPDATE_LESSON';
export const ADD_VIDEO_LESSON = 'ADD_VIDEO_LESSON';
export const ADD_VIDEO_LESSON2 = 'ADD_VIDEO_LESSON2';

export const SHOW = 'SHOW';
export const HIDE = 'HIDE';
