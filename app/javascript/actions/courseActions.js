import { FETCH_COURSES,
         FETCH_COURSE,
         NEW_COURSE,
         DELETE_COURSE
 } from './types';
import axios from 'axios';


export const fetchCourses = () => dispatch => {
  console.log('fetchCourses');
  axios.get('/api/courses')
      .then(courses =>
        dispatch({
          type: FETCH_COURSES,
          payload: courses
        })
      )
}

export const createCourse = (courseData) => dispatch => {
  console.log('createCourse');
  axios
  .post('/api/courses', courseData)
  .then(course =>
    dispatch({
      type: NEW_COURSE,
      payload: course
    })
  )
}

export const fetchCourse = (courseData) => dispatch => {
  const id = courseData.id;
  axios
  .get(`api/courses/${id}`)
  .then(course =>
    dispatch({
      type: FETCH_COURSE,
      payload: course
    })
  )
}

export const deleteCourse = (courseData) => dispatch => {
  const id = courseData.id;
  // $.ajax({
  //       url: `/api/courses/${id}/`,
  //       beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
  //       type: 'DELETE',
  //       success(response) {
  //         dispatch({
  //           type: DELETE_COURSE
  //         })
  //       }
  // });
  axios
  .delete(`/api/courses/${id}`, courseData)
  .then(course =>
    dispatch({
      type: DELETE_COURSE
    })
  )
}
