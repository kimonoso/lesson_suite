import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactPlayer from 'react-player';

import { addVideoLesson } from '../../actions/lessonActions';
import LoadingSpinner from '../shared/LoadingSpinner';


class AddVideoLesson extends Component {
  constructor(props) {
    super(props);

    this.videoUploadHandler = this.videoUploadHandler.bind(this);
  }

  videoUploadHandler(e) {
    e.preventDefault();

    console.log(e);

    const lesson = {
      video: e.target.value,
      course_id: this.props.course.data.params.id
    }

    this.props.addVideoLesson(lesson);
  }

  render() {

    var this_component = (
      <div>
        <div className="field">
          <div className="file is-large is-boxed has-name">
            <label className="file-label">
              <input className="file-input"
                     type="file"
                     name="video"
                     onChange={this.videoUploadHandler}
                   />
              <span className="file-cta">
                <span className="file-icon">
                  <i className="fas fa-video" />
                </span>
                <span className="file-label">
                  Upload Video Lesson
                </span>
              </span>
              <span className="file-name">
                Screen Shot 2017-07-29 at 15.54.25.png
              </span>
            </label>
          </div>
        </div>
      </div>
    );

    var loading = (<LoadingSpinner/>);
    var data = this.props.lesson.data;

    if (data) {
      return loading;
    } else {
      return this_component;
    }
  }
}

const mapStateToProps = state => ({
  course: state.courses.item,
  lesson: state.lessons.item
})

export default connect(mapStateToProps, { addVideoLesson })(AddVideoLesson);
