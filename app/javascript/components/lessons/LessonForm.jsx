import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { passCsrfToken } from '../../util/helpers';
import axios from 'axios';
import ReactQuill, { Quill } from 'react-quill';

import { createLesson } from '../../actions/lessonActions';
import { showToggle, hideToggle } from '../../actions/toggleActions';
import { deleteCourse } from '../../actions/courseActions';

import AddVideoLesson from './AddVideoLesson';
import VideoTesting from './VideoTesting';


class LessonForm extends Component {
  constructor(props){
    super(props);

    this.state = {
      name: null,
      type: null,
      body: null,
      video: null,
      course_id: null
    }

    this.onChange = this.onChange.bind(this);
    this.onTextChange = this.onTextChange.bind(this);
    this.onNextLesson = this.onNextLesson.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    passCsrfToken(document, axios);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.lesson == {}) {
      const lesson = {
        name: '',
        type: '',
        body: '',
        video: '',
        course_id: props.course.id
      }
      props.createLesson(lesson);
    }
    return null;
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onTextChange(value) {
    this.setState({ body: value });
  }

  onNextLesson(e) {
    e.preventDefault();

    const lesson = {
      name: this.state.name,
      type: this.state.type,
      body: this.state.body,
      video: this.state.video,
      course_id: this.props.course.data.params.id
    }

    this.props.createLesson(lesson);

  }

  onSubmit(e) {
    this.onNextLesson(e);
  }

  handleCancel(e) {
    e.preventDefault();

    var course = this.props.course.data.params;

    console.log(course);

    this.props.hideToggle('createLesson');
    this.props.showToggle('courses');
    this.props.deleteCourse(course);
  }

  render() {

    var course = this.props.course.data;

    if (course) {
      var course_title = course.params.name;
    } else {
      var course_title = null;
    }

    const quotes = [
      'The sky is the limit',
      "Let's get creative",
      "There's no one stopping you.",
      "Stay awesome. Get creative."
    ]

    var random_quote = quotes[Math.floor(Math.random()*3)];

    return (
      <div className="columns courseForm">
        <div className="column is-10 is-offset-1">
          <div className='content course-form'>
            <h5>Create Your Lesson</h5>
            <VideoTesting />
            <form onSubmit={this.onNextLesson}>
              <div className="field">
                <input
                  name='name'
                  className="input"
                  type='text'
                  placeholder='Lesson Name'
                  onChange={this.onChange}
                />
              </div>
              <div className="field quill">
                <ReactQuill
                  placeholder={random_quote}
                  onChange={this.onTextChange}
                  tabIndex={1}
                />
              </div>
              <div className="control">
                <label className="radio">
                  <input
                    type="radio"
                    name='type'
                    value='TextLesson'
                    defaultChecked={true}
                    onClick={this.onChange}
                  /> Text
                </label>
                <label className="radio">
                  <input
                    type="radio"
                    name='type'
                    value='VideoLesson'
                    onClick={this.onChange}
                  /> Video
                </label>
              </div>
              <div className="field is-grouped">
                <div className="control">
                  <button
                    className='button is-info'
                    type='submit'
                    >
                    Next Lesson
                  </button>
                </div>
                <div className="control">
                  <a
                    className='button is-success'
                    type='submit'
                    onClick={this.onSubmit}
                    >
                    Submit
                  </a>
                </div>
                <div className="control">
                  <a
                    className='button is-warning'
                    onClick={this.handleCancel}
                    >
                    Cancel
                  </a>
                </div>

              </div>

            </form>
          </div>
        </div>
      </div>
    );
  }
}

LessonForm.propTypes = {
  createLesson: PropTypes.func.isRequired,
  showToggle: PropTypes.func.isRequired,
  hideToggle: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  course: state.courses.item,
  lesson: state.lessons.item
})

const mapDispatchToProps = {
  createLesson,
  deleteCourse,
  showToggle,
  hideToggle
}


export default connect(mapStateToProps, mapDispatchToProps)(LessonForm);
