import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactPlayer from 'react-player';

import { addVideoLesson2 } from '../../actions/lessonActions';
import LoadingSpinner from '../shared/LoadingSpinner';

import ActiveStorageProvider from 'react-activestorage-provider';



class VideoTesting extends Component {
  constructor(props) {
    super(props);
  }


  render() {

    return (
      <ActiveStorageProvider
        endpoint={{
          path: '/api/lesson',
          model: 'Lesson',
          attribute: 'video',
          method: 'POST',
        }}
        onSubmit={user => this.setState({ vie })}
        render={({ handleUpload, uploads, ready }) => (
          <div>
            <input
              type="file"
              disabled={!ready}
              onChange={e => handleUpload(e.currentTarget.files)}
            />

            {uploads.map(upload => {
              switch (upload.state) {
                case 'waiting':
                  return <p key={upload.id}>Waiting to upload {upload.file.name}</p>
                case 'uploading':
                  return (
                    <p key={upload.id}>
                      Uploading {upload.file.name}: {upload.progress}%
                    </p>
                  )
                case 'error':
                  return (
                    <p key={upload.id}>
                      Error uploading {upload.file.name}: {upload.error}
                    </p>
                  )
                case 'finished':
                  return (
                    <p key={upload.id}>Finished uploading {upload.file.name}</p>
                  )
              }
            })}
          </div>
        )}
      />
    )
  }
}

const mapStateToProps = state => ({
  course: state.courses.item,
  lesson: state.lessons.item
})

export default connect(mapStateToProps, { addVideoLesson2 })(VideoTesting);
