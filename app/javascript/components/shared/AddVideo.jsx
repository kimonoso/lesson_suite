import React, { Component } from 'react';
import ReactPlayer from 'react-player';

export default class AddVideo extends Component {
  render() {
    return (
      <div>
        <div className="field">
          <div className="file is-large is-boxed has-name">
            <label className="file-label">
              <input className="file-input" type="file" name="resume" />
              <span className="file-cta">
                <span className="file-icon">
                  <i className="fas fa-video" />
                </span>
                <span className="file-label">
                  Upload Video Lesson
                </span>
              </span>
              <span className="file-name">
                Screen Shot 2017-07-29 at 15.54.25.png
              </span>
            </label>
          </div>
        </div>
      </div>
    )
  }
}
