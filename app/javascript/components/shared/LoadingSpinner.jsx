import React, { Component } from 'react';

export default class LoadingSpinner extends Component {
  render() {
    return (
      <div className="lds-css ng-scope">
        <div
          className="lds-spinner"
          >
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    )
  }
}
