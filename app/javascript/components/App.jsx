import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import NavBar from './shared/NavBar';
import Home from './Home';

import CourseParent from './courses/CourseParent';

import store from  '../store';

import LessonForm from './lessons/LessonForm';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <NavBar />
            <Route path='/' exact component={Home} />
            <Route path='/courses/' exact component={CourseParent} />
            <Route path='/lesson/' exact component={LessonForm} />

          </div>
        </Router>
      </Provider>
    );
  }
}
