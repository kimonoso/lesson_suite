import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCourses } from '../../actions/courseActions';

class Courses extends Component {
  constructor(props){
    super(props);
    this.props.fetchCourses();
  }

  render() {
    var data = this.props.courses.data;
    if (typeof data !=='undefined') {
      var courseItems = Array.from(data.courses).map(course => (
        <div
          key={course.id}
        >
          <h3>{course.name}</h3>
          <p>{course.description}</p>
        </div>
      ));
      var base = (
        <div className="columns courseForm">
          <div className="column is-4 is-offset-4">
            <div className='content course-form'>
              <h1>Courses</h1>
              {courseItems}
            </div>

          </div>
        </div>
      )
    } else {
      var base = null
    }

    return base;

  }
}

const mapStateToProps = state => ({
  courses: state.courses.items,
})


export default connect(mapStateToProps, { fetchCourses })(Courses);
