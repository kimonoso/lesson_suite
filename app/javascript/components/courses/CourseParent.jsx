import React, { Component } from 'react';
import { connect } from 'react-redux';

import Courses from './Courses';
import Course from './Course';
import CourseForm from './CourseForm';
import CourseNav from './CourseNav';
import LessonForm from '../lessons/LessonForm';
import {Toggle} from '../shared/Toggle';

class CourseParent extends Component {
  render() {

    return (
      <div>
        <CourseNav />
        <Toggle id='courses'>
          <Courses />
        </Toggle>
        <Toggle id='createCourse'>
          <CourseForm />
        </Toggle>
        <Toggle id='course'>
          <Course />
        </Toggle>
        <Toggle id='createLesson'>
          <LessonForm />
        </Toggle>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  course: state.courses.item
})

export default connect(mapStateToProps, null)(CourseParent);
