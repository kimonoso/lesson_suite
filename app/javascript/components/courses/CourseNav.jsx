import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { showToggle, hideToggle } from '../../actions/toggleActions';

class CourseNav extends Component {
  constructor(props){
    super(props);
    this.renderCourses = this.renderCourses.bind(this);
    this.createCourse = this.createCourse.bind(this);
  }

  renderCourses(e){
    e.preventDefault();

    this.props.showToggle('courses');
    this.props.hideToggle('createCourse');
    this.props.hideToggle('createLesson');
    this.props.hideToggle('course');
  }

  createCourse(e){
    e.preventDefault();

    this.props.showToggle('createCourse');
    this.props.hideToggle('courses');
    this.props.hideToggle('createLesson');
    this.props.hideToggle('course');
  }

  render() {
    return (
      <nav className="navbar courses is-primary" role="navigation" aria-label="main navigation">
        <div className="navbar-brand">
          <a
            className="navbar-item"
            onClick={this.renderCourses}
            >
            Courses
          </a>

          <a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>

        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            <a
              className='navbar-item'
              onClick={this.createCourse}
              >
              Create a Course
            </a>
          </div>
        </div>
      </nav>
    );
  }
}

CourseNav.propTypes = {
  showToggle: PropTypes.func.isRequired,
  hideToggle: PropTypes.func.isRequired
}

const mapDispatchToProps = {
  showToggle,
  hideToggle
}

export default connect(null, mapDispatchToProps)(CourseNav);
