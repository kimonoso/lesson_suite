import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createCourse } from '../../actions/courseActions';
import { passCsrfToken } from '../../util/helpers';
import axios from 'axios';
import { showToggle, hideToggle } from '../../actions/toggleActions';


import LessonForm from '../lessons/LessonForm';

class CourseForm extends Component {
  constructor(props){
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    passCsrfToken(document, axios);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const course = {
      name: this.state.name,
      description: this.state.description
    }
    this.props.createCourse(course);
    this.props.hideToggle('createCourse');
    this.props.showToggle('course');
    this.props.showToggle('createLesson');
  }

  render() {
    const course_form = (
      <div className="columns courseForm">
        <div className="column is-4 is-offset-4">
          <div className='content course-form' id='course-form'>
            <h1>Create New Course</h1>
            <form onSubmit={this.onSubmit}>
              <div className="field">
                <label className="label">Name</label>
                <input
                  name='name'
                  className="input"
                  type='text'
                  placeholder='Enter the name of your course'
                  onChange={this.onChange}
                />
              </div>
              <div className="field">
                <label className="label">
                  Description
                </label>
                <textarea
                  name='description'
                  cols="30"
                  rows="10"
                  className="textarea"
                  placeholder='How would you describe your course?'
                  onChange={this.onChange}
               />
              </div>
              <button
                className='button is-info'
                type='submit'
                >
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    )
    return  course_form;

  }
}

CourseForm.propTypes = {
  createCourse: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  course: state.courses.item
})

const mapDispatchToProps = {
  createCourse,
  showToggle,
  hideToggle
}

export default connect(mapStateToProps, mapDispatchToProps)(CourseForm);
