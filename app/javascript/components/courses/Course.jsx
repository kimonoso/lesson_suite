import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCourse } from '../../actions/courseActions';

class Course extends Component {
  constructor(props){
    super(props);
  }

  render() {

    var course = this.props.course.data;

    if (course) {
      console.log(course.params);
      var course_title = course.params.name;
    } else {
      var course_title = null;
    }
    return(
      <section className="hero is-primary">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">
              {course_title}
            </h1>
            <h2 className="subtitle">
              INSERT USER NAME
            </h2>
          </div>
        </div>
      </section>
    )
  }
}

const mapStateToProps = state => ({
  course: state.courses.item
})

export default connect(mapStateToProps, null)(Course);
