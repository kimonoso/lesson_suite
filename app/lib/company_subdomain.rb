# frozen_string_literal: true

class CompanySubdomain
  class << self
    def matches?(request)
      Company.all.pluck(:subdomain).include? request.subdomain
    end
  end
end
