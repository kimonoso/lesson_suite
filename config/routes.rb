# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: 'user/sessions' }

  constraints CompanySubdomain do
    root to: 'react#index'
    resources :react, except: [:index]
  end

  namespace :api, defaults: {format: 'json'} do
    resources :courses
    resources :lessons
  end


  root to: 'static#home'
  match '*path', to: 'react#index', via: :all
end
