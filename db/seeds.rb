# frozen_string_literal: true

company = Company.create!(name: 'FullSuite', subdomain: 'fullsuite')
Company.create!(name: 'Kruze Consulting', subdomain: 'kruzeconsulting')

user = User.create!(
  first_name: '1 Employee',
  last_name: 'FullSuite',
  email: '1@fullsuite.ph',
  password: 'miko123',
  password_confirmation: 'miko123',
  company: company
)

Admin.create!(
  first_name: 'Admin',
  last_name: 'FullSuite',
  email: 'admin@fullsuite.ph',
  password: 'miko123',
  password_confirmation: 'miko123',
  company: company
)

Instructor.create!(
  first_name: 'Instructor',
  last_name: 'FullSuite',
  email: 'instructor@fullsuite.ph',
  password: 'miko123',
  password_confirmation: 'miko123',
  company: company
)

100.times do |i|
  course = Course.create(
    name: "Course #{i}",
    description: "Course #{i} description",
    review: rand(1..5),
    review_count: 1,
    user_id: user.id
  )

  5.times do |ii|
    Lesson.create(
      name: "Course #{i}: Lesson #{ii}",
      body: "Lesson #{ii} description",
      type: 'TextLesson',
      course_id: course.id
    )
  end
end
