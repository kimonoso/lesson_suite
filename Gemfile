# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

gem 'administrate', '~> 0.11.0'
gem 'anime_js_rails'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'bulma-rails', '~> 0.7.5'
gem 'coffee-rails', '~> 4.2'
gem 'devise'
gem 'gritter', '~> 1.2'
gem 'haml-rails', '~> 2.0'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'kaminari', '~> 1.1', '>= 1.1.1'
gem 'pg'
gem 'puma', '~> 3.11'
gem 'pundit', '~> 2.1'
gem 'rails', '~> 5.2.3'
gem 'react-rails'
gem 'sass-rails', '~> 5.0'
gem 'simple_form'
gem 'turbolinks', '~> 5'
gem 'twilio-ruby'
gem 'uglifier', '>= 1.3.0'
gem 'webpacker'
gem 'wicked'
gem 'wysiwyg-rails'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'capybara'
  gem 'database_cleaner'
  gem 'dotenv-rails'
  gem 'factory_bot_rails'
  gem 'haml_lint', require: false
  gem 'rspec-rails', '~> 3.0'
  gem 'rubocop'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'web-console', '>= 3.3.0'
end
